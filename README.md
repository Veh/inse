![Inse](https://i.imgur.com/duurJUO.png)

Inse är ett program för att hjälpa till med inventering av varor, och informera om utgångsdatum mm. För tillfället är programet en prototyp och inte fungerande. Byggt med nwjs och databas i IndexedDB med hjälp av Dexie.

[nwjs.io](http://nwjs.io)
[dexie](http://dexie.org)