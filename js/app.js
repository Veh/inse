var win = nw.Window.get();
//var moment = require("moment");

// Set title to use todays date
var today = moment().format("YYYY-MM-DD");
document.title = "Inse " + today;

// Toggle the alert
function triggerAlert() {
	document.querySelectorAll(".alert")[0].classList.toggle("hide");
}

function view(page) {
	nw.Window.open(page + ".html");
}

// ========================
// MENU
//

// Create menu
var menu = new nw.Menu({type: 'menubar'});

// Create file menu
var fileMenu = new nw.Menu();

// Append menu items
fileMenu.append(new nw.MenuItem({
	label: "Visa inventarie",
	key: "1",
	modifiers: "ctrl",
	click: function(){
		console.log("1")
	}
}));

fileMenu.append(new nw.MenuItem({
	label: "Visa statistik",
	key: "2",
	modifiers: "ctrl",
	click: function(){
		console.log("2")
	}
}));

fileMenu.append(new nw.MenuItem({
	label: "Inställningar",
	key: "3",
	modifiers: "ctrl",
	click: function() {
		view("settings");
	}
}));
menu.append(new nw.MenuItem({
	label: "Inse",
	submenu: fileMenu
}));

// Create help menu
var helpMenu = new nw.Menu();

// Append menu items
helpMenu.append(new nw.MenuItem({label: "Hjälp"}));
helpMenu.append(new nw.MenuItem({label: "Om"}));
menu.append(new nw.MenuItem({
label: "Hjälp",
submenu: helpMenu
}));
nw.Window.get().menu = menu;

// DATEPICKER
var picker = new Pikaday({
    field: document.getElementById("datepicker"),
    firstDay: 1,
    i18n: {
    previousMonth : 'Föregående månad',
    nextMonth     : 'Nästa månad',
    months        : ['Januari','Februari','Mars','April','Maj','Juni','Juli','Augusti','September','Oktober','November','December'],
    weekdays      : ['Söndag','Måndag','Tisdag','Onsdag','Torsdag','Fredag','Lördag'],
    weekdaysShort : ['Sön','Mån','Tis','Ons','Tor','Fre','Lör']
	},
	onClose: function() {
		document.getElementById("noDate").focus();
	}
});

// NO DATE BUTTON
var btn = document.getElementById("noDate");
var form = document.getElementById("datepicker");
function enableDate() {
    form.value = "";
    form.disabled = false;
    btn.onclick = addNoDate;
}
function addNoDate() {
    form.value = "XXXX-XX-XX";
    form.disabled = true;
    btn.onclick = enableDate;
}

btn.onclick = addNoDate;


// Check all checkboxes when check is clicked
var check = document.getElementById("checkAll");
var boxes = document.querySelectorAll(".tableCheck");
var boxesLength = boxes.length;

function removeEntry() {
	document.getElementById("removeEntry").classList.toggle("hide");
}

function checkAll() {
	if (check.checked) {
		for (var i = 0; i < boxesLength; i++) {
			boxes[i].checked = true;
		}
	} else {
		for (var i = 0; i < boxesLength; i++) {
			boxes[i].checked = false;
		}
	}
}


check.onclick = checkAll;

// Do a removeEntry(); call when any checkbox is checked to display the functions to remove or whatnot