// INSE
// INVENTARIE SE <-> ES EUROSTAR

//
// APP SETTINGS
//

// var moment = require("moment"); // something with require and my module structure makes this not work that good
// var Pikaday = require("pikaday"); //npm outdated, lagging behind, not working as of 2016-04-09

var inv = (function () {
    var s;
    return {
        settings: {
            
        },
        init: function() {
            
        },
        read: function() {
            
        },
        write: function() {
            
        }
        
    }    
})

var inse = (function () {
    var s; // private alias to settings
    return {
        settings: {
            name: "Inse",
            win: nw.Window.get(),
            date: moment().format("YYYY-MM-DD")
        },
        init: function() {
            s = this.settings;
            document.title = s.name + " " + s.date;
            winMenu.init();
            alert.init();
            datePicker.init();
            check.init();
        }
    }
})(); //inse.init()


//
// WINDOW HANDLER
//
var view = (function (page) {
    var s;
    return {
        open: function(page) {
            nw.Window.open(page + ".html");
        }
    }
})(); //view.open("settings")


//
// WINDOW MENU
//

var winMenu = (function () {
    return {
        init: function() {
            var menu = new nw.Menu({type: "menubar"});
            
            // File menu
            var fileMenu = new nw.Menu();
            fileMenu.append(new nw.MenuItem({
                label: "Visa inventarie",
                key: "1",
                modifiers: "ctrl",
                click: function(){
                    console.log("1")
                }
            }));

            fileMenu.append(new nw.MenuItem({
                label: "Visa statistik",
                key: "2",
                modifiers: "ctrl",
                click: function(){
                    console.log("2")
                }
            }));

            fileMenu.append(new nw.MenuItem({
                label: "Inställningar",
                key: "3",
                modifiers: "ctrl",
                click: function() {
                    view.open("settings");
                }
            }));

            menu.append(new nw.MenuItem({
                label: "Inse",
                submenu: fileMenu
            }));

            // Help menu
            var helpMenu = new nw.Menu();
            helpMenu.append(new nw.MenuItem({label: "Hjälp"}));
            helpMenu.append(new nw.MenuItem({label: "Om"}));
            menu.append(new nw.MenuItem({
                label: "Hjälp",
                submenu: helpMenu
            }));

            // Create Menu
            nw.Window.get().menu = menu;
        }
    }
})(); // winMenu.init()


//
// ALERT BOX
//

var alert = (function() {
    var s;
    return {
        settings: {
            alert: document.querySelector(".alert")
        },
        init: function() {
            s = this.settings;
            s.alert.classList.toggle("hide");
        }
    }
})(); // alert.init();


//
// DATEPICKER
//

var datePicker = (function() {
    var s; //private alias to settings
    return {
        settings: {
            noDate: document.getElementById("noDate"),
            dateForm: document.getElementById("datepicker")
        },
        init: function() {
            s = this.settings;
            var picker = new Pikaday({
                field: s.dateForm,
                firstDay: 1,
                i18n: {
                previousMonth : 'Föregående månad',
                nextMonth     : 'Nästa månad',
                months        : ['Januari','Februari','Mars','April','Maj','Juni','Juli','Augusti','September','Oktober','November','December'],
                weekdays      : ['Söndag','Måndag','Tisdag','Onsdag','Torsdag','Fredag','Lördag'],
                weekdaysShort : ['Sön','Mån','Tis','Ons','Tor','Fre','Lör']
                },
                onClose: function() {
                    s.noDate.focus();
                }
            });
        }
    }
})(); // datePicker.init()


//
// CHECKBOX LOGIC
//
var check = (function() {
    var s; //private alias to settings
    return {
        settings: {
            noDate: document.getElementById("noDate"),
            dateForm: document.getElementById("datepicker"),
            masterCheck: document.getElementById("checkAll"),
            listCheck: document.querySelectorAll(".tableCheck")
        },
        init: function() {
            s = this.settings;
            s.noDate.onclick = this.disableDateForm;
            s.masterCheck.onclick = this.checkAll;
        },
        enableDateForm: function() {
            s.dateForm.value = "";
            s.dateForm.disabled = false;
            s.noDate.onclick = check.disableDateForm;
        },
        disableDateForm: function() {
            s.dateForm.value = "XXXX-XX-XX";
            s.dateForm.disabled = true;
            s.noDate.onclick = check.enableDateForm;
        },
        checkAll: function() {
            if(s.masterCheck.checked) {
                for (var i = 0; i < s.listCheck.length; i++) {
                    s.listCheck[i].checked = true;
                }
            } else {
                for (var i = 0; i < s.listCheck.length; i++) {
                    s.listCheck[i].checked = false;
                    
                }
            }
        }
    }
})(); // check.init()
inse.init();